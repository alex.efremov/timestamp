Timestamp microservice. Receive a Unix timestamp as a parameter and returns natural date in json format.

Example:

http://localhost/timestamp?s=1510572362

Result: 

{
    "naturalDate": "13 November 2017"
}

To run this microservice you need glassfish application server (or any other Java EE compliant application server).

1. Package application running *mvn clean package*
2. Deploy application on glassfish server *asadmin deploy target/timestamp-1.0.war*