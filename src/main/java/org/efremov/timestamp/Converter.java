/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.timestamp;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

/**
 *
 * @author efrem
 */
public class Converter {

    /**
     *
     * @param timestamp string representing a unix timestamp
     * @return a LocalDate or null
     */
    public static LocalDate timestampToLocalDate(String timestamp) {
        try {
            return Instant.ofEpochSecond(Long.parseLong(timestamp)).atZone(ZoneId.systemDefault()).toLocalDate();
        }
        catch (NumberFormatException ex) {
            System.out.println("timestamp parameter cannot be converted to LocalDate");
            return null;
        }
    }
}
