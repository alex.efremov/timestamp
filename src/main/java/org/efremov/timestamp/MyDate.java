/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.timestamp;

import java.time.LocalDate;
import javax.json.bind.annotation.JsonbDateFormat;

/**
 *
 * @author efrem
 */
public class MyDate {
    
    @JsonbDateFormat("dd MMMM yyyy")
    LocalDate naturalDate = null;  

    public MyDate() {
    }

    public MyDate(LocalDate naturalData) {
        this.naturalDate = naturalData;
    }

    public LocalDate getNaturalDate() {
        return naturalDate;
    }

    public void setNaturalDate(LocalDate naturalDate) {
        this.naturalDate = naturalDate;
    }
}
