/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.timestamp;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author efrem
 */
public class TimestampConverterTest {
    
    private Jsonb jsonb;
    
    public TimestampConverterTest() {
    }
   
    
    @Before
    public void setUp() {
        JsonbConfig nillableConfig = new JsonbConfig().withNullValues(true);
        this.jsonb = JsonbBuilder.create(nillableConfig);
    }
    
    /**
     * Test of convert method, of class TimestampConverter.
     */
    @Test
    public void shouldConvertNumberToDate() {
        String timestamp = "1510575043";
        String expResult = "{\"naturalDate\":\"13 November 2017\"}";
        String result = jsonb.toJson(new MyDate(Converter.timestampToLocalDate(timestamp)));
        
        assertEquals(expResult, result);
    }
    
    @Test
    public void shouldNotConvertStringToDate() {
        String timestamp = "notconvertable";
        String expResult = "{\"naturalDate\":null}";
        String result = jsonb.toJson(new MyDate(Converter.timestampToLocalDate(timestamp)));
        
        assertEquals(expResult, result);
    }
}
